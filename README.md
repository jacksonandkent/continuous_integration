# J&K continuous integration+deployement
Utiliser ce package pour tirer profit du l'intégration et déploiement continue au sein de J&K.

## Installation

### Ajouter le package au composer.json
Editer le /composer.json à la racine de votre projet (le créer s'il n'existe pas) pour ajouter dans require-dev :

    "jacksonandkent/continuous_integration": "dev-master"

Et ajouter un noeud en frère de require :

    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/jacksonandkent/continuous_integration.git"
        }
    ]

Exemple de composer.json ne contenant que les informations nécessaires à l'installation :

	{
	    "require-dev": {
	        "jacksonandkent/continuous_integration": "dev-master"
	    },
	    "repositories": [{
	        "type": "vcs",
	        "url": "https://bitbucket.org/jacksonandkent/continuous_integration.git"
	    }]
	}

Dire à composer d'installer ce package :

    composer update jacksonandkent/continuous_integration

Installer le package :

    php ./vendor/jacksonandkent/continuous_integration/src/install.php

## Utilisation

### Fichier de configuration
Après avoir l'installation, éditer le fichier jandk_cicd_config.json.

Contenus :

##### Debug
- VERBOSE : Permet de demander au script d'afficher plus d'information dans les log de bitbucket
##### Slack
- SLACK_CHANNEL : Le channel slack sur lequel publier les informations de build
- SLACK_WEBHOOKURL : L'URL nécessaire si on souhaite publier sur slack. (La même url peut être utiliser sur plusieurs projets)
##### Commandes
- COMMAND_START : Commande à lancer au démarrage du script
- COMMAND_SETUP_FOR_TESTS : Commande à lancer pour préparer l'environnement de test
- COMMAND_UNIT_TEST : Commande à lancer pour lancer les tests unitaires
- COMMAND_FEATURE_TEST : Commande à lancer pour lancer les tests d'intégration
- COMMAND_BROWSER_TEST : Commande à lancer pour lancer les tests fonctionnels (au sein d'un navigateur)
- COMMAND_BEFORE_ZIP_CREATION : Commande à lancer avant la création du zip (ex: nettoyage des fichiers inutils une fois le site déployé)
- COMMAND_BEFORE_ZIP_UPLOAD : Commande à lancer avant l'upload du zip
- COMMAND_REMOTE_AFTER_ZIP_EXTRACTION : Commande à lancer sur le serveur où a lieu le déploiement

##### Recette
- STAGING_SERVER_ADDRESS : Addresse du serveur de recette (recette3.jandk.fr ?)
- STAGING_JANDK_PATH : Chemin jusqu'au répertoire de recette J&K
- STAGING_JANDK_URL : URL pour accéder à la recette J&K
- STAGING_CLIENT_PATH : Chemin jusqu'au répertoire de recette client
- STAGING_CLIENT_URL : URL pour accéder à la recette client

#### Aide sur les commandes
##### Nom de l'environnement
Utilisez %ENV_NAME% pour ajouter le nom de l'environnement dans une commande.
Exemple : 

	cp .env.%ENV_NAME% .env

##### Docker
Pour éxécuter des commandes au sein d'un docker, il faut les passer en paramètre de bash. 
Par exemple, pour un laradock dont le container principal s'appel workspace, pour y lancer un composer install, la commande serait :

	docker-compose exec -T workspace bash -c \"composer install\"

# Todo

- ~~Faire fonctionner la communication avec le channel slack s'il est fourni~~
- ~~Lancer les tests unitaires~~
- ~~Lancer les tests d'intégration~~
- ~~Compiler les assets~~
- ~~Déployer en recette si les informations sont fournies~~
- ~~Déployer en recette client si demandé et que les informations sont fournies~~
- ~~Nettoyer les fichiers de ce package avant déploiement~~
- Faire fonctionner les tests navigateur
- Déployer en production - Autre git
- Déployer en production - FTP
- Déployer en production - E-mail