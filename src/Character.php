<?php

namespace JacksonAndKent;

class Character
{

    private $characters = [
        [
            "name"    => "Marty McFly",
            "phrases" => [
                "start" => "Doc !! Doc !! La pipeline s'est lancée sur *".CURRENT_BRANCH."* !!",
            ],
            "images"  => [
                "https://upload.wikimedia.org/wikipedia/en/d/d8/Michael_J._Fox_as_Marty_McFly_in_Back_to_the_Future%2C_1985.jpg",
                "http://filmgarb.com/wp-content/uploads/film-back_to_the_future-1985-marty_mcfly-michael_j_fox-jackets-red_down_vest-595x335.jpg",
                "https://i.imgflip.com/svjhi.jpg?a414216",
                "http://i.dailymail.co.uk/i/pix/2015/07/10/00/2A641C0D00000578-0-image-m-8_1436485278253.jpg",
            ],
        ],
        [
            "name"    => "Axel Folley",
            "phrases" => [
                "start" => "Eh ouais mec ! Elle est lancée sur *".CURRENT_BRANCH."* !!",
            ],
            "images"  => [
                "https://static.cineclick.com.br/sites/adm/uploads/banco_imagens/49/400x400_1375020409.jpg",
                "http://vignette3.wikia.nocookie.net/beverlyhillscop/images/a/ae/Axel.png/revision/latest?cb=20130101203557",
                "http://th.cineblog.it/BhaTna-Zvjr4oj5eynHlEQywOlM=/fit-in/655xorig/http://media.cineblog.it/6/697/stasera-in-tv-su-italia-1-beverly-hills-cop-con-eddie-murphy-3.jpg",
                "http://cdn.shopify.com/s/files/1/0255/3587/t/3/assets/blog_Axel-Foley-Porsche-Carrera.jpg?34359",
                "https://pmcdeadline2.files.wordpress.com/2014/05/axel-foley__140502214154.jpg",
                "https://s-media-cache-ak0.pinimg.com/originals/f1/6a/2f/f16a2f0e6d7f99171971bb8b22b34c67.jpg",
            ],
        ],
        [
            "name"    => "Angus MacGyver",
            "phrases" => [
                "start" => "Du scotch, un chewing-gum et hop c'est lancée sur *".CURRENT_BRANCH."* !",
            ],
            "images"  => [
                "https://apparentdip.files.wordpress.com/2007/02/a0a8a-bio1x.jpg",
                "http://www.badassoftheweek.com/macgyver2.jpg",
                "http://vignette3.wikia.nocookie.net/macgyver/images/3/30/0808281240172.jpg/revision/latest?cb=20090620203901",
                "https://s3.amazonaws.com/gs-geo-images/ec37c91b-1ad9-4948-b771-9652d1ac1865.jpg",
                "http://www.angusmacgyver.fr/photos/saison6/6x22%20(1).jpg",
                "http://www.doyoubuzz.com/var/users/_/2014/7/23/15/752701/avatar/658366/avatar_cp_big.jpg?t=1490385916",
                "https://3.bp.blogspot.com/-29ZnCOCBEkA/VztT0u0TtYI/AAAAAAAAKEc/Q_lKO2-7cwo5tbaXI7BPUnyQqQSsuRJcQCLcB/s1600/macgyver.jpg",
            ],
        ],
        [
            "name"    => "Gaius Baltar",
            "phrases" => [
                "start" => "Ce n'est pas moi qui ai lancé la pipeline sur *".CURRENT_BRANCH."* !",
            ],
            "images"  => [
                "https://upload.wikimedia.org/wikipedia/en/thumb/5/51/Baltar_Season_3.jpg/220px-Baltar_Season_3.jpg",
                "http://images.buddytv.com/btv_2_505540886_0_350_10000_-1_/baltar.jpg",
                "http://photos1.blogger.com/blogger/6786/525/1600/Gaius%20Baltar.1.jpg",
                "http://www.sadgeezer.com/Images/Battlestar-Galactica/Baltar/bgpic_06.jpg",
                "http://img12.deviantart.net/6d4d/i/2011/133/3/d/gaius_baltar_t_shirt_design_by_lunai-d3g892a.png",
                "http://www.soapb.com/wp-content/uploads/2009/02/poster.jpg",
                "https://i.kinja-img.com/gawker-media/image/upload/s--SPkWN4nl--/18s3yuhlrasdjjpg.jpg",
                "http://i.onionstatic.com/avclub/3435/05/16x9/1200.jpg",
            ],
        ],
        [
            "name"    => "Jon Snow",
            "phrases" => [
                "start" => "I know nothing about *".CURRENT_BRANCH."*.",
            ],
            "images"  => [
                "http://www.buzzdefou.com/wp-content/uploads/2016/08/Game-of-Thrones-Finale-Jon-Snow-Dead-Killed.jpg",
                "https://s-media-cache-ak0.pinimg.com/736x/75/16/ea/7516ea7c26b59356f07e0a7d251a79bf.jpg",
                "https://pbs.twimg.com/profile_images/743138182607175680/ZJzktgBk_400x400.jpg",
                "https://static.giantbomb.com/uploads/original/3/39164/2865551-reasons-people-love-game-thrones-jon-snow-video.jpg",
                "http://cdn.images.express.co.uk/img/dynamic/20/590x/secondary/Jon-Snow-477022.jpg",
            ],
        ],
        [
            "name"    => "Caprica 6",
            "phrases" => [
                "start" => "Is *".CURRENT_BRANCH."* alive ?",
            ],
            "images"  => [
                "https://upload.wikimedia.org/wikipedia/en/5/5e/Number_Six_Tricia_Helfer.jpg",
                "http://assets1.ignimgs.com/2014/12/09/tricia-helfer-as-caprica-6jpg-886993_1280w.jpg",
                "http://www.writeups.org/wp-content/uploads/Number-Six-Battlestar-Galactica-Tricia-Helfer.jpg",
                "https://thetop5five.files.wordpress.com/2012/07/tricia-helfer-5.jpg",
            ],
        ],
        [
            "name"    => "Billy Keikeya",
            "phrases" => [
                "start" => "Romuald m'a demandé de lancer la pipeline sur *".CURRENT_BRANCH."*.",
            ],
            "images"  => [
                "http://www.thescifiworld.net/img/interviews/paul_campbell_04.jpg",
                "http://media.battlestarwiki.org/images/thumb/1/16/Mini-Billy.jpg/200px-Mini-Billy.jpg",
                "http://www.aveleyman.com/Gallery/ActorsC/40968.gif",
                "https://a4-images.myspacecdn.com/images03/35/424f01b58b7b4de480b12678ffd81e50/300x300.jpg",
                "https://i.kinja-img.com/gawker-media/image/upload/s--_cdlxD3k--/17mtlq2bd9653jpg.jpg",
            ],
        ],
        [
            "name"    => "Michael Scott",
            "phrases" => [
                "start" => "Hello, i'm Michael Scarn, special agent investingating your pipeline on *".CURRENT_BRANCH."* .oO(that's what she said)",
            ],
            "images"  => [
                "https://upload.wikimedia.org/wikipedia/en/d/dc/MichaelScott.png",
                "http://az616578.vo.msecnd.net/files/2016/01/13/6358830572699376411733442098_6358177813696988401291296824_michael-scott-the-office-9.jpg",
                "https://s-media-cache-ak0.pinimg.com/originals/ec/ea/2a/ecea2a4d6aed1b243159c3ce9356083e--dunder-mifflin-michael-scott.jpg",
                "http://cdn.postgradproblems.com/wp-content/uploads/2013/11/da1ff1205cd2fb13cb84d10e07bd246a.jpg",
                "http://az616578.vo.msecnd.net/files/2015/08/24/6357600113572837231773916132_michael-scott-s-top-tantrums.png",
                "http://cdn.playbuzz.com/cdn/72597fa5-ba37-41e4-8d5c-6f4e645e1c3c/da3c0247-19a0-4166-a916-c1550a0eca26.png",
                "http://az616578.vo.msecnd.net/files/2016/12/11/636170134339550520-411827784_41a1acdb-7afc-4737-b0d2-01f8fd964b82.jpg",
                "https://storybookstorage.s3.amazonaws.com/items/images/000/132/272/original/Michael_Scott_Prison.png?1441814158",
                "http://cdn.playbuzz.com/cdn/72597fa5-ba37-41e4-8d5c-6f4e645e1c3c/1a2bdced-b759-46a6-b0db-7fe73c559716.jpg",
                "http://filmpopper.com/wp-content/uploads/2013/01/Michael-Scott-Steve-Carrell-The-Office.jpg",
                "http://starcasm.net/wp-content/uploads/2010/10/Michael_Scott_MacGruber.jpg?ggnoads",
            ],
        ],
        [
            "name"    => "Michael Knight",
            "phrases" => [
                "start" => "Kit, lances la pipeline sur *".CURRENT_BRANCH."* !",
            ],
            "images"  => [
                "http://vignette2.wikia.nocookie.net/knightrider/images/7/7e/MichaelKnightOS.jpg/revision/latest?cb=20080211005807&path-prefix=en",
                "http://vignette2.wikia.nocookie.net/p__/images/c/cf/Ss3230890_-_photograph_of_david_hasselhoff_as_michael_knight_from_knight_rider_available_in_4_sizes_framed_or_unframed_buy_now_at_starstills_22000.jpg/revision/latest?cb=20120308204624&path-prefix=protagonist",
                "http://www.dailymobile.net/wp-content/uploads/2014/05/Michael-Knight.jpg",
                "http://www.snakkle.com/wp-content/uploads/2012/11/David-Hasselhoff-GC.jpg",
                "http://4.bp.blogspot.com/-wIZUy9h0DJs/VYRDDPe5SlI/AAAAAAACHPI/9KwjbITUvFU/s640/Michael-Knight-Laying-down.jpg",
                "http://ecx.images-amazon.com/images/I/41-yFx2aL3L.jpg",
                "https://i.ytimg.com/vi/CP_i5C4bByA/hqdefault.jpg",
            ],
        ],
        [
            "name"    => "Yoda",
            "phrases" => [
                "start" => "La pipeline sur *".CURRENT_BRANCH."* j'ai lancé.",
            ],
            "images"  => [
                "http://vignette2.wikia.nocookie.net/starwars/images/d/d6/Yoda_SWSB.png/revision/latest?cb=20150206140125",
                "http://digitalspyuk.cdnds.net/16/38/768x403/gallery-1474472774-yoda-008.jpg",
                "http://lesgicques.fr/wp-content/uploads/2016/04/yoda-lesgicques.jpg",
                "https://s-media-cache-ak0.pinimg.com/originals/ea/d1/0f/ead10fbb45d3d4d4783679ef43e9e72a.jpg",
                "http://nerdist.com/wp-content/uploads/2016/11/Screen-Shot-2016-11-26-at-4.14.07-PM.png",
            ],
        ],
        [
            "name"    => "Luke Skywalker",
            "phrases" => [
                "start" => "J'ai pécho ma soeur et lancé la pipeline sur *".CURRENT_BRANCH."* !",
            ],
            "images"  => [
                "http://vignette3.wikia.nocookie.net/starwars/images/1/15/Luke_Skywalker_Ep_7_SWCT.png/revision/latest?cb=20161230081329",
                "https://vignette1.wikia.nocookie.net/starwars/images/d/d9/Luke-rotjpromo.jpg/revision/latest/scale-to-width-down/350?cb=20091030151422",
                "https://vignette3.wikia.nocookie.net/starwars/images/c/c1/Luke_on_Endor.jpg/revision/latest/scale-to-width-down/170?cb=20100622233847",
                "http://media.gq.com/photos/56da0101062ab67b27facbd2/16:9/pass/luke-skywalker-gay-.jpg",
                "http://media.melty.fr/article-3158530-ratio265_1020/universite-campus-etudiant-fac-star-wars.jpg",
            ],
        ],
        [
            "name"    => "Darth Vader",
            "phrases" => [
                "start" => "*".CURRENT_BRANCH."*, je suis ton père",
            ],
            "images"  => [
                "https://media3.popsugar-assets.com/files/2015/06/09/930/n/1922398/ef0d5ecd_shutterstock_239009758.xxxlarge_2x.jpg",
                "https://img.actualidadcine.com/wp-content/uploads/2016/05/Darth-Vader_phixr.jpg",
                "http://assets2.ignimgs.com/2015/08/06/darth-vader-crossed-arms-1280jpg-88461e1280wjpg-67c0c2_1280w.jpg",
                "http://static.srcdn.com/wp-content/uploads/Star-Wars-Darth-Vader-Wallpaper.jpg",
                "http://static.boredpanda.com/blog/wp-content/uploads/2015/11/the-daily-life-of-darth-vader-is-my-latest-365-day-photo-project-33__880.jpg",
                "https://s-media-cache-ak0.pinimg.com/736x/73/db/97/73db97c0c4a9c9b009d69f21ea48ecdc.jpg",
            ],
        ],
        [
            "name"    => "Kamoulox",
            "phrases" => [
                "start" => "Je culbute un *".CURRENT_BRANCH."* et serre la main de Chirac.",
            ],
            "images"  => [
                "https://i.ytimg.com/vi/HiIO9YrvuTE/hqdefault.jpg",
                "https://www.tuxboard.com/photos/2012/08/kamouloz-lintergrale.jpg",
                "https://i.ytimg.com/vi/44bud9HhDW0/hqdefault.jpg",
                "https://i.ytimg.com/vi/vv4s5Y2dO8I/hqdefault.jpg",
                "https://i.ytimg.com/vi/CkTEE5M-9Vg/hqdefault.jpg",
                "https://i.ytimg.com/vi/8bDmeeGVNvc/hqdefault.jpg",
            ],
        ],
        [
            "name"    => "Han Solo",
            "phrases" => [
                "start" => "I shot the pipeline on *".CURRENT_BRANCH."* first.",
            ],
            "images"  => [
                "http://fansided.com/files/2015/08/han-solo-who-scruffy-looking-620x.jpg",
                "http://cdn-static.denofgeek.com/sites/denofgeek/files/2016/01/han-solo-main.jpg",
                "https://vignette2.wikia.nocookie.net/starwars/images/f/fc/HanChillinAtChalmuns-ANH.jpg/revision/latest/scale-to-width-down/210?cb=20111204212526",
                "http://cdn.mos.cms.futurecdn.net/cpzXQFXEM2H99ZqWALUsZk.jpg",
                "https://lumiere-a.akamaihd.net/v1/images/Han-Solo_1d08eb2e.jpeg?region=0%2C51%2C1600%2C800",
                "http://img.actucine.com/wp-content/uploads/2017/04/han-solo.jpg",
                "http://www.journaldugeek.com/files/2017/01/Han-Solo.jpg",
            ],
        ],
        [
            "name"    => "Clara Morgane",
            "phrases" => [
                "start" => "Salut beau gosse, tu veux mettre ta pipeline sur mon *".CURRENT_BRANCH."* ?",
            ],
            "images"  => [
                "http://www.radiometal.com/images/posts/2009/11/journalduhard.jpg",
                "http://www.fredzone.org/wp-content/uploads/2010/09/clara-morgane.jpg",
                "https://www.lhommetendance.fr/wp-content/uploads/2015/10/Lemon-Curve-Maison-Close-H%C3%B4tel-Diva1.jpg",
                "http://media.melty.fr/article-692179-fb/clara-morgane-calendrier-2012.jpg",
                "http://static1.purepeople.com/articles/6/10/34/76/@/891563-clara-morgane-950x0-2.jpg",
            ],
        ],
        [
            "name"    => "Jean-Luc Godard",
            "phrases" => [
                "start" => "OOuuuiiii, la pipeliiiinneee, sur *".CURRENT_BRANCH."*...blablabla ce que je dis est mieux blablabla...",
            ],
            "images"  => [
                "http://www.bfi.org.uk/sites/bfi.org.uk/files/styles/full/public/image/jean-luc-godard-scratching-head-00m-ht7.jpg?itok=J5ozqTPy",
                "http://www.theyshootpictures.com/images/godardjeanluc1.jpg",
                "http://www.newwavefilm.com/images/jean-luc-godard.jpg",
                "http://www.tasteofcinema.com/wp-content/uploads/2016/01/best-jean-luc-godard-films.jpg",
            ],
        ],
    ];

    public function __construct()
    {
    	// On pioche au hasard un personnage
    	shuffle($this->characters);
    	$this->character = $this->characters[0];
    	
    	$this->name = $this->character["name"];
    	
    	shuffle($this->character["images"]);
    	$this->image = $this->character["images"][0];
    }

    public function startPhrase()
    {
    	return $this->character["phrases"]["start"];
    }
}
