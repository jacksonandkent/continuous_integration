<?php

function setupYML()
{
    if (!file_exists("bitbucket-pipelines.yml")) {
    	$package_path = dirname(__FILE__);

    	$destfile = "./bitbucket-pipelines.yml";

        copy($package_path."/resources/bitbucket-pipelines.yml", $destfile);

        echo("- bitbucket-pipelines.yml cree.");
    } else {
        echo("- bitbucket-pipelines.yml existant, fichier source non copie.");
    }
}

function setupConfig()
{
    if (!file_exists("jandk_cicd_config.json")) {
    	$package_path = dirname(__FILE__);

        copy($package_path."/resources/jandk_cicd_config.json", "./jandk_cicd_config.json");

        echo("- jandk_cicd_config.json cree.");
    } else {
        echo("- jandk_cicd_config.json existant, configuration de base non creee.");
    }
}

echo "--------------------------\n";
echo "Installation des elements necessaires a l'integration et le deploiement continue.";
echo "\n--------------------------\n\n";
setupYML();
echo "\n";
setupConfig();
echo "\n\n";