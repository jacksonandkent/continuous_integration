<?php

/*
Pour tester ce script :
docker-compose up -d
docker-compose run ubuntu bash

export SSH_RECETTE_PASSWORD=forjnkstaffonly
php src/run.php staging_jandk
 */

require getcwd() . "/vendor/autoload.php";

// On vérifie qu'on a les paramètres nécessaire
if (!isset($argv[1])) {
    echo "Vous devez indiquer l'environnement sur lequel déployer en paramètre. Exemple : run.php staging_jandk";
    exit();
} else {
    define("ENV_NAME", $argv[1]);
}

// On lit le fichier de configuration
$config = new JacksonAndKent\Config();

// DEBUG
define("VERBOSE", $config->v("VERBOSE"));

// On récupère le nom de la branche
define("CURRENT_BRANCH", isset($_SERVER["BITBUCKET_BRANCH"]) ? $_SERVER["BITBUCKET_BRANCH"] : "no_branch");

// On instancie le slack (rien ne se passera si les informations de configuration ne sont pas fournies)
$slack = new JacksonAndKent\Slack($config->v("SLACK_WEBHOOKURL"), $config->v("SLACK_CHANNEL"));
$slack->sendStart();

// On lance la commande de début
execCommand("Commande du début", "COMMAND_START");

// On lance la commande de setup des tests
execCommand("Commande de setup des tests", "COMMAND_SETUP_FOR_TESTS");

// On lance la commande des tests unitaires
execCommand("Commande des tests unitaires", "COMMAND_UNIT_TEST");

// On lance la commande des tests fonctionnels
execCommand("Commande des tests fonctionnels", "COMMAND_FEATURE_TEST");

// On lance la commande des tests de navigateur
execCommand("Commande des tests de navigateur", "COMMAND_BROWSER_TEST");

// On déploie là où demandé
$deploy = new JacksonAndKent\Deploy($config, $slack);
switch (ENV_NAME) {
    case 'staging_jandk':
        $deploy->onStagingJandK(ENV_NAME, $config->v("STAGING_JANDK_PATH"), $config->v("STAGING_JANDK_URL"));
        break;

    case 'staging_client':
        $deploy->onStagingJandK(ENV_NAME, $config->v("STAGING_CLIENT_PATH"), $config->v("STAGING_CLIENT_URL"));
        break;

    case 'production':
        $deploy->onProduction();
        break;
}

// Fin
echo "************* Log à afficher\n";
$slack->send("Fin");

/**
Fonctions support
 */
function execAndLog($logMessage, $command = null, $ignore_error = false)
{
    global $slack;

    if ($command == null) {
        $command = $logMessage;
    }

    // On affiche le message de log
    $logMessage = str_replace(getenv("SSH_RECETTE_PASSWORD"), "***", $logMessage);
    echo "- " . $logMessage . "\n";

    // On execute la commande et on en récupère l'output
    $output = null;
    exec($command, $output, $returnVar);
    $command = str_replace(getenv("SSH_RECETTE_PASSWORD"), "***", $command);

    // Si on a demandé le verbose, on affiche les paramètres passés
    if (VERBOSE) {
        echo "Message : " . $logMessage . "\n";
        echo "Commande : " . $command . "\n";
        echo "Return var : " . $returnVar . "\n";
    }

    // S'il y a une erreur, on la reporte
    if ($returnVar > 0 && !$ignore_error) {
        reportError("Echec de la commande suivante : " . $command, $output);
    }
    // Sinon et qu'on a demandé le verbose, on affiche quand même le retour de la commande
    elseif (VERBOSE) {
        foreach ($output as $outputLine) {
            echo $outputLine . "\n";
        }
    }

    return [
        "returnVar" => $returnVar,
        "output"    => $output,
    ];
}

function sshExecAndLog($stagingPath, $logMessage, $command = null, $ignore_error = false)
{
    global $config;

    if ($command == null) {
        $command = $logMessage;
    }

    // On se connecte en ssh
    $sshCommandPrefix = "sshpass -p " . getenv("SSH_RECETTE_PASSWORD") . " ssh -oStrictHostKeyChecking=no web@" . $config->v("STAGING_SERVER_ADDRESS") . " 'cd " . $stagingPath . " && ";
    execAndLog("SSH - " . $logMessage, $sshCommandPrefix . $command . "'", $ignore_error);
}

function execCommand($message, $commandConstantName, $ignore_error = false)
{
    global $config;

    $command = $config->v($commandConstantName);

    if ($command != "") {
        $command = str_replace("'", "\'", $command);
        $command = str_replace("%ENV_NAME%", ENV_NAME, $command);

        execAndLog($message, $command, $ignore_error);
    }
}

function execRemoteCommand($stagingPath, $message, $commandConstantName, $ignore_error = false)
{
    global $config;

    $command = $config->v($commandConstantName);

    if ($command != "") {
        $command = str_replace("'", "\'", $command);
        $command = str_replace("%ENV_NAME%", ENV_NAME, $command);

        sshExecAndLog($stagingPath, $message, $command, $ignore_error);
    }
}

function reportError($message, $output = [])
{
    global $hasError, $slack;
    $hasError = true;

    echo $message . "\n";
    $slack->sendLater($message, false);

    foreach ($output as $outputLine) {
        echo $outputLine . "\n";
        $slack->sendLater($outputLine, false);
    }

    $slack->send("Fin du reporting de l'erreur.");

    exit(1);
}
