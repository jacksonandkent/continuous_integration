<?php

namespace JacksonAndKent;

class Slack
{
    private $toBeSlacked;
    private $fields;

    private $channel;
    private $webHookURL;
    private $username;
    private $thumbnail;

    public function __construct($webHookURL, $channel)
    {

        if (substr($channel, 0, 1) != "#") {
            $channel = "#" . $channel;
        }

        $this->channel    = $channel;
        $this->webHookURL = $webHookURL;
    }

    public function send($message, $showFull = true)
    {
        // Si on a pas défini les infos de configuration, on envoi rien
        if ($this->webHookURL == "" || $this->channel == "" || $this->channel == "#") {
            return;
        }

        $message           = $this->toBeSlacked . $message;
        $message           = urlencode($message);
        $this->toBeSlacked = "";

        // Si le message est trop long, on le découpe
        if (strlen($message) > 2048) {
            $messages = str_split($message, 2048);
            
            foreach ($messages as $splittedMessage) {
                $this->send($splittedMessage, $showFull);
            }

            return;
        }

        // Si le full est demandé, on met l'attachement
        if ($showFull) {
            $data = [
                "channel"     => $this->channel,
                "username"    => $this->character->name,
                "icon_url"    => $this->character->image,
                "attachments" => [
                    0 => [
                        "fallback"  => $message,
                        "text"      => $message,
                        "fields"    => $this->fields,
                        "image_url" => $this->character->image,
                    ],
                ],
            ];
        } else {
            $data = [
                "channel"  => "#" . $this->channel,
                "text"     => $message,
                "username" => $this->character->name,
                "icon_url" => $this->character->image,
            ];
        }

        $payload = "payload=" . json_encode($data);

        $ch = curl_init($this->webHookURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result != "ok") {
            echo "Erreur à l'envoi du slack : '" . $result . "'\n";
            echo "webHookURL : " . $this->webHookURL;
            echo "Data : \n";
            print_r($data);
            echo "Post data : \n" . $payload;
        }

        return $result . "\n";

    }

    public function sendLater($message)
    {
        $this->toBeSlacked .= $message . "\n";
    }

    public function addField($title, $value, $short = true)
    {
        $this->fields[] = [
            "title" => $title,
            "value" => $value,
            "short" => $short,
        ];
    }

    public function sendStart()
    {
        // On récupère un personnage
        $this->character = new Character();

        // On informe du début
        $this->send($this->character->startPhrase(), false);
    }
}
