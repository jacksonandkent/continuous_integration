<?php

namespace JacksonAndKent;

class Config
{
    private $config;

    public function __construct($filename = null)
    {
        if ($filename == null) {
            $configPath = getcwd() . "/jandk_cicd_config.json";
        }

        if (!file_exists($configPath)) {
            echo "Erreur au chargement de la configuration, fichier inexistant. Peut-etre faut-il lancer l'installation ?";
            exit(1);
        }

        $this->config = json_decode(file_get_contents($configPath));
    }

    public function v($index){
    	if(!isset($this->config->{$index})){
            $message = $index." n'a pas ete trouve dans la configuration.";
            echo $message;
            reportError($message);
    	}

    	return $this->config->{$index};
    }
}
