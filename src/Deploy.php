<?php

namespace JacksonAndKent;

use Symfony\Component\Yaml\Yaml;

class Deploy
{
    public function __construct($config, $slack)
    {
        $this->config    = $config;
        $this->slack     = $slack;
    }

    public function onStagingJandK($envName, $stagingPath, $stagingURL = "")
    {
        // On vérifie qu'on est pas sur windows
        if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
            echo "- Deployement only on linux with apt-get\n";
            $this->slack->addField("Déploiement " . $envName, "Déploiement depuis windows impossible");
            return;
        }

        // On vérifie qu'on a le mot de passe du serveur de recette
        if (getenv("SSH_RECETTE_PASSWORD") == "") {
            echo "- Environnement variable SSH_RECETTE_PASSWORD is not set\n";
            $this->slack->addField("Déploiement " . $envName, "Mot de passe du serveur de recette manquant");
            return;
        }

        // On vérifie qu'on a bien le chemin où déployer
        if ($stagingPath == "") {
            echo "- Staging J&K path missing\n";
            $this->slack->addField("Déploiement " . $envName, "Aucun chemin fourni");
            return;
        }

        // On execute la commande de preparation des fichiers
        execCommand("Commande avant zip", "COMMAND_BEFORE_ZIP_CREATION");

        // On fait le zip        
        execAndLog("Installation de zip", "apt-get install -y zip");
        $zipFilename = $envName . "_" . date("Ymd-Hmi") . ".zip";
        execAndLog("zip -0 -r " . $zipFilename . " *");

        // Commande pré-upload
        execCommand("Commande avant upload", "COMMAND_BEFORE_ZIP_UPLOAD");

        // On install de quoi se connecter en ssh au serveur
        execAndLog("Installation de sshpass", "apt-get install -y sshpass");

        // On vide le répertoire d'installation - On ignore les erreurs car un docker peut créer des fichiers avec un autre user et on s'en fiche.
        sshExecAndLog($stagingPath, "Emptying the folder " . $stagingPath, "rm -rf " . $stagingPath . "/*", true);

        // On install lftp
        execAndLog("apt-get install -y lftp");

        $lftpCommands = '
            set sftp:auto-confirm yes
            open -u web,' . getenv("SSH_RECETTE_PASSWORD") . ' -p 22 sftp://'.$this->config->v("STAGING_SERVER_ADDRESS").'
            put ' . $zipFilename . ' -o ' . $stagingPath . '/' . $zipFilename . '
            exit';
        file_put_contents("upload_zip_file", $lftpCommands);

        // On utilise lftp pour envoyer le fichier
        execAndLog("Sync to " . $stagingPath, "lftp -f upload_zip_file");

        // On dézip - On ignore les erreurs car un docker peut créer des fichiers avec un autre user et on s'en fiche.
        sshExecAndLog($stagingPath, "Unzipping " . $zipFilename, "unzip -o " . $zipFilename, true);

        // On supprime le zip
        sshExecAndLog($stagingPath, "Removing uploaded zip " . $zipFilename, "rm " . $zipFilename);

        // Commande pré-upload
        execRemoteCommand($stagingPath, "Commande après extraction du zip", "COMMAND_REMOTE_AFTER_ZIP_EXTRACTION");

        // On invite à voir le site sur l'url de déploiement
        if ($stagingURL != "") {
            $this->slack->addField("URL de déploiement", $stagingURL, false);
        } else {
            $this->slack->addField("URL de déploiement", "Aucune URL fournie");
        }

        // On supprime le fichier d'upload
        unlink("upload_zip_file");

        // On supprime le zip
        unlink($zipFilename);

    }

    public function deployDockerOnStagingJandk($envName)
    {
        // Si on est pas sur un environnement docker, on ne fait rien
        if (!$this->docker->isDocker()) {
            echo "- No docker environnement\n";
            return null;
        }

        // On vérifie qu'on a un docker-compose.yml pour l'environnement
        if (!$this->docker->environnementFileExists($envName)) {
            echo "- Staging J&K docker-compose file missing (docker-compose.yml." . $envName . ")\n";
            reportError("Docker-compose.yml." . $envName . " absent, déploiement annulé.");
        }

        // On copie le docker-compose de l'environnement
        execAndLog("Copying staging docker-compose file", "mv " . $this->docker->envFilename($envName) . " " . $this->docker->envFilename());

        // On récupère les ports publics demandé pour les containers
        $dockerCompose = Yaml::parse(file_get_contents("docker-compose.yml"));

        // On boucle sur les containers configurés
        foreach ($dockerCompose["services"] as $service) {
            if (is_array($service) && array_key_exists("ports", $service)) {
                foreach ($service["ports"] as $portMap) {
                    $ports = explode(":", $portMap);

                    // On récupère les ports à ouvrir
                    $portsToAdd["public"] = $ports[0];
                    $portsToAdd["internal"] = $ports[1];
                    $portsToMap[] = $portsToAdd;
                }
            }
        }

        // On fait le htaccess pour le proxy sur ces ports
        if (count($portsToMap) > 0) {
            // On récupère les ports à mapper sur le serveur
            $portsToServerMapping = explode(";", $this->config->v("DOCKER_APPLICATION_EXPOSED_PORTS_MAPPING_TO_SERVER_VIA_HTACCESS"));
            foreach($portsToServerMapping as $portsString){
                list($serverPort, $exposedPort) = explode(":", $portsString);
                $portsToMapOnServersPort[$exposedPort] = $serverPort;
            }

            $htaccessContent = "RewriteEngine On\n";
            foreach ($portsToMap as $ports) {
                $redirectFrom = isset($portsToMapOnServersPort[$ports["public"]]) ? $portsToMapOnServersPort[$ports["public"]] : $ports["public"];
                $redirectTo = $ports["public"];

                $htaccessContent .= 'RewriteCond "%{SERVER_PORT}" "'.$redirectFrom.'"'."\n";
                $htaccessContent .= "RewriteRule ^/?(.*)$ http://localhost:" . $redirectTo . "/$1 [L,P]\n";
            }

            file_put_contents(".htaccess", $htaccessContent);
        }

        // On ajoute les fichiers au prochain commit
        execAndLog("git add docker-compose.yml");
        execAndLog("git add .htaccess");

    }
}
